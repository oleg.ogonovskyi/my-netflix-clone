import React from "react";
import './footer.css';
import { FaFacebook, FaTwitter, FaInstagram, FaGithub, FaYoutube } from 'react-icons/fa';

const Footer = () => {
    return (
        <div className="footer-wrapper">
            <div className="icon facebook">
                <div className="tooltip">Facebook</div>
                <span><FaFacebook /></span>
            </div>
            <div className="icon twitter">
                <div className="tooltip">Twitter</div>
                <span><FaTwitter /></span>
            </div>
            <div className="icon instagram">
                <div className="tooltip">Instagram</div>
                <span><FaInstagram /></span>
            </div>
            <div className="icon github">
                <div className="tooltip">Github</div>
            <span><FaGithub /></span>
            </div>
            <div className="icon youtube">
                <div className="tooltip">Youtube</div>
                <span><FaYoutube /></span>
            </div>
        </div>
    )
};

export default Footer; 