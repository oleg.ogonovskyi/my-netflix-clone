import React from 'react';
import './movies.css';
import { NavLink } from 'react-router-dom';

const Movies = ({title, allMovies, genre}) => {
    
    const onWheelScroll = (e) => {
        document.querySelector('.movie-logo-wrapper').scrollLeft += e.deltaY;   
    };

    if (genre === 'CRIME') {
        allMovies = allMovies.filter(el => el.genres.includes('Crime'));
    } else if (genre === 'DRAMA') {
        allMovies = allMovies.filter(el => el.genres.includes('Drama'));
    } else if (genre === 'THRILLER') {
        allMovies = allMovies.filter(el => el.genres.includes('Thriller'));
    } else if (genre === 'ROMANCE') {
        allMovies = allMovies.filter(el => el.genres.includes('Romance'));
    } else if (genre === 'MEDICAL') {
        allMovies = allMovies.filter(el => el.genres.includes('Medical'));
    }  else if (genre === 'SUPERNATURAL') {
        allMovies = allMovies.filter(el => el.genres.includes('Supernatural'));
    }  else if (genre === 'ACTION') {
        allMovies = allMovies.filter(el => el.genres.includes('Action'));
    }  else if (genre === 'SCIENCE-FICTION') {
        allMovies = allMovies.filter(el => el.genres.includes('Science-Fiction'));
    } else if (genre === 'ADVENTURE') {
        allMovies = allMovies.filter(el => el.genres.includes('Adventure'));
    } else if (genre === 'FAMILY') {
        allMovies = allMovies.filter(el => el.genres.includes('Family'));
    }

    return (
        <div className='movie-line'>
            <h2 id={genre} className='title-text'>{title}</h2>
            <div className='movie-logo-wrapper' onWheel={onWheelScroll} >
                {
                    allMovies.map(el => {
                        return (
                            <div className='item-img-wrapper'>
                                <NavLink to={'./' + el.id + '/' + el.name.toLowerCase().replace(/[:|&]/g, '').replace(/[^a-zA-Z0-9]/g, '-')} >
                                    <img 
                                        src={el.image.medium} 
                                        alt={el.name}
                                        key={el.id} 
                                        className='movie-img'
                                    />
                                    <span className='movie-rating' key={el.name}>&#9733; {el.rating.average}</span>
                                </NavLink>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
};

export default Movies;