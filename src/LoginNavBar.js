import React from 'react';
import { NavLink } from 'react-router-dom';
import './loginNavBar.css';

const LoginNavBar = () => {
    return (
        localStorage.getItem('logedIn') === 'true' &&
        <div className='my-nav-bar-wrapper'>
            <ul className='my-menu-wrapper'>
                <NavLink to='./my-page' >
                    <button className='my-menu-button'>My page</button>
                </NavLink>
                <NavLink to='./find' >
                    <button className='my-menu-button'>Find</button>
                </NavLink>
                
            </ul>
        </div>
    )
};

export default LoginNavBar;