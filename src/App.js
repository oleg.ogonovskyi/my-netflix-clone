import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./Login";
import Register from "./Register";
import Reset from "./Reset";
import Dashboard from "./Dashboard";
import MainPage from './MainPage';
import requests from './requests';
import Movie from './Movie';

function App() {
  return (
    <div className="app">
      <Router>
        <Switch>
          <Route exact path='/'>
            <MainPage requests={requests}/>
          </Route>
          <Route path='/:id/:movie'>
            <Movie />
          </Route>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route exact path="/register">
            <Register />
          </Route>
          <Route exact path="/reset">
            <Reset />
          </Route>
          <Route exact path="/dashboard">
            <Dashboard />  
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;