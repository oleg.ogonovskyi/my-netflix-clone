import React from "react";
import './signInButtons.css';
import { NavLink } from "react-router-dom";
import SearchInput from "./SearchInput";

const SignInButtons = (props) => {
    
    return (
        <div className='sign-in-wrapper'>
            <div className='search-button-wrapper'>
                <NavLink to={
                    localStorage.getItem('logedIn') === 'true' ? '/dashboard' : '/login'
                }>
                    <button className='sign-in-button'>
                        {
                            localStorage.getItem('logedIn') === 'true' ? 'Log Out' : 'Log In'
                        }    
                    </button> 
                </NavLink>
            </div>
            <SearchInput allMovies={props.allMovies}/> 
        </div>
    )
};

export default SignInButtons;