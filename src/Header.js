import { NavLink } from "react-router-dom";
import './header.css';
import HeaderNavButton from "./HeaderNavButton";
import SignInButtons from "./SignInButtons";

const Header = ({allMovies, value}) => {
    const movie = allMovies[Math.floor(Math.random() * allMovies.length)];
    
    return (
        <header>
            <div className='header-wrapper'>
                <HeaderNavButton />
                <div 
                    className='header-logo' 
                    style={{backgroundImage: `url(${movie.image?.original || movie.image?.medium})`}
                }>
                    <SignInButtons allMovies={allMovies} />
                    <NavLink to={'./' + movie.id + '/' + movie.name.toLowerCase().replace(/[:|&]/g, '').replace(/[^a-zA-Z]/g, '-')}>
                        <div className='header-info'>
                            <h1 className='header-movie-name'>{movie.name}</h1>
                            <span className='header-movie-rating'>&#9733; {movie?.rating?.average}</span>
                            <p className='header-summary'>{movie.summary?.replaceAll(/(<([^>]+)>)/gi, '')}</p>
                            <p className='header-schedule'>{movie.schedule?.time}, on {movie.schedule?.days} at {movie.network?.name}</p>
                        </div>
                   </NavLink>
                </div>
            </div>
        </header>
    )
};

export default Header;