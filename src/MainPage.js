import React from "react";
import Header from "./Header";
import Movies from "./Movies";
import axios from "./axios";
import requests from "./requests";
import Footer from "./Footer";
import Loader from 'react-loader-spinner';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

class MainPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            allMovies: [],
        }
    }
    
    componentDidMount() {
            axios.get(requests.fetchAllMovies)
                .then(response => {
                    this.setState({
                        loading: false,
                        allMovies: response.data.filter(el => el.id !== 64),
                    })
                })
    }

    render() {
        let resultToRender;
        if (this.state.loading) {
            resultToRender = (
                <div style={{position: 'fixed', top: '40%', left: '40%'}}>
                    <Loader 
                        color="rgb(229, 9, 20)"
                        type="Plane"
                        height={100}
                        width={100}
                    />
                </div>
            )
        } else {
            if (localStorage.getItem('logedIn') === 'true') {
                resultToRender = (
                    <>
                        <div className="App">
                            <Header allMovies={this.state.allMovies} />
                            <Movies title='ALL MOVIES' allMovies={this.state.allMovies} genre='ALL'/>
                            <Movies title='CRIME' allMovies={this.state.allMovies} genre='CRIME'/>
                            <Movies title='DRAMA' allMovies={this.state.allMovies} genre='DRAMA'/>
                            <Movies title='THRILLER' allMovies={this.state.allMovies} genre='THRILLER'/>
                            <Movies title='ROMANCE' allMovies={this.state.allMovies} genre='ROMANCE'/>
                            <Movies title='MEDICAL' allMovies={this.state.allMovies} genre='MEDICAL'/>
                            <Movies title='SUPERNATURAL' allMovies={this.state.allMovies} genre='SUPERNATURAL'/>
                            <Movies title='ACTION' allMovies={this.state.allMovies} genre='ACTION'/>
                            <Movies title='SCIENCE-FICTION' allMovies={this.state.allMovies} genre='SCIENCE-FICTION'/>
                            <Movies title='ADVENTURE' allMovies={this.state.allMovies} genre='ADVENTURE'/>
                            <Movies title='FAMILY' allMovies={this.state.allMovies} genre='FAMILY'/>
                        </div>
                        <Footer />
                    </>
                )
            } else {
                resultToRender = (
                    <>
                        <div className="App">
                            <Header allMovies={this.state.allMovies} />
                        </div>
                        <Footer />
                    </>
                )
            }  
        }
        
        return (
           resultToRender            
        )
    }
    
};

export default MainPage;