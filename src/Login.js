import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { auth, signInWithEmailAndPassword, signInWithGoogle } from "./firebase";
import { useAuthState } from "react-firebase-hooks/auth";
import "./Login.css";
import Loader from "react-loader-spinner";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [user, loading] = useAuthState(auth);
  const history = useHistory();
  
  useEffect(() => {
    if (loading) {
      <Loader 
        color="rgb(229, 9, 20)"
        type="Plane"
        height={100}
        width={100}
      />
      return;
    }
    if (user) history.replace("/dashboard");
  }, [user, loading, history]);

  return (
    <div className="login">
      <div className="login__container">
        <p className='log_in_warning'>Please login in order to continue</p>
        <input
          type="text"
          className="login__textBox"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          placeholder="E-mail Address"
        />
        <input
          type="password"
          className="login__textBox"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          placeholder="Password"
        />
        <button
          className="login__btn"
          onClick={() => {
            localStorage.setItem('logedIn', true)
            return signInWithEmailAndPassword(email, password)
          }}
        >
          Login
        </button>
        <button className="login__btn login__google" onClick={signInWithGoogle}>
          Login with Google
        </button>
        <div>
          <Link to="/reset">Forgot Password</Link>
        </div>
        <div>
          Don't have an account? <Link to="/register">Register</Link> now.
        </div>
        <div>
          <Link to="/">Go back to main page</Link>
        </div>
        
      </div>
    </div>
  );
}

export default Login;