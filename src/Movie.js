import React from 'react';
import axios from './axios';
import requests from './requests';
import './movie.css'
import Footer from './Footer';
import SignInButtons from './SignInButtons';
import MoviePageTitle from './MoviePageTitle';
import MovieSummary from './MovieSummary';
import FollowNotification from './FollowNotification';
import Loader from 'react-loader-spinner';

class Movie extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            movie: [],
            allMovies: [],
            favorites: [],
            follow: '',
        }
        this.handleFavorites = this.handleFavorites.bind(this);
    }
    
    componentDidMount() {
        let href = window.location.href.split('/').slice(-2, -1)[0];
        axios.get(requests.fetchAllMovies)
            .then(response => {
                let item = response.data.filter(el => el.id === +href)[0];
                this.setState({
                    loading: false,
                    movie: item,
                    allMovies: response.data.filter(el => el.id !== 64)
                })
                document.title = this.state.movie.name;
            })
    }

    handleFavorites(e) {
        if (localStorage.getItem('logedIn') === 'true') {
            switch(e.target.innerHTML) {
                case 'Follow':    
                    return this.setState({
                        ...this.state,
                        favorites: [...this.state.favorites, this.state.movie.name],
                        follow: 'Follow'
                    })   
                case 'Unfollow': 
                    return this.setState({
                        ...this.state,
                        favorites: this.state.favorites.filter(el => el !== this.state.movie.name),
                        follow: 'Unfollow'
                    })
                default:
                    return this.state;
            }
        } else {
            window.location = '/login';
        }   
    }

    render() {
        console.log(this.props.state);
        let resultToRender;
        if (this.state.loading) {
            resultToRender = (
                <div style={{position: 'fixed', top: '40%', left: '40%'}}>
                    <Loader 
                        color="rgb(229, 9, 20)"
                        type="Plane"
                        height={100}
                        width={100}
                    />
                </div>
            )
        } else {
            resultToRender = (
                <div className='movie-page-wrapper'>
                    <div className='movie-page-logo' style={{backgroundImage: `url(${this.state.movie.image.original})`}}>
                            {
                                <button className='sign-in-button favorite' onClick={this.handleFavorites}>
                                    { this.state.favorites.includes(this.state.movie.name) ? 'Unfollow' : 'Follow'}
                                </button> 
                            }
                    </div>
                    <FollowNotification movie={this.state.favorites} followed={this.state.follow} />
                    <div className='movie-page-info'>
                        <SignInButtons allMovies={this.state.allMovies}/>
                                <div className='movie-id-wrapper'>
                                    <MoviePageTitle title={this.state.movie.name} />
                                    <div className='movie-id-logo'>
                                        <a href={this.state.movie.officialSite} target='_blank'></a>
                                    </div>
                                    <div className='movie-id-info'>
                                        <p>Language: {this.state.movie.language}</p>
                                        <p>Official Site:
                                            &nbsp;<span><a href={this.state.movie.officialSite} target='_blank'>{this.state.movie.officialSite}</a></span> 
                                        </p>
                                        <p>Premiered: {this.state.movie.premiered}</p>
                                        <p className='movie-genres'>Genres: {this.state.movie.genres.join(', ')}</p>
                                        <p>Runtime: {this.state.movie.runtime} minutes</p>
                                        <p>
                                            Watch at {this.state.movie.schedule.time} every {this.state.movie.schedule.days[0]} at 
                                            &nbsp;{this.state.movie.network.name} ({this.state.movie.network.country.code} time)
                                        </p>
                                    </div>
                                    <MovieSummary summary={this.state.movie.summary.replaceAll(/(<([^>]+)>)/gi, '')}/>   
                                </div>
                        <Footer />
                    </div>
                </div>
            );
        }

        return (
            resultToRender
        )
    }
};

export default Movie;