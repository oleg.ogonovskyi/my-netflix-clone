import React from "react";
import { NavLink } from "react-router-dom";

const MenuItem = ({menuItemText}) => {
    return (
        localStorage.getItem('logedIn') === 'true' ? 
            <a href={`#${menuItemText}`} className='menu-item-wrapper'>
                <span className='menu-item-span'>{menuItemText}</span>
            </a> : 
            <NavLink to='./login' className='menu-item-wrapper'>
                <span className='menu-item-span'>{menuItemText}</span>
            </NavLink>
    )
};

export default MenuItem;