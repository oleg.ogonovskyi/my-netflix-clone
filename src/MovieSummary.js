import React from 'react';

const MovieSummary = (props) => {
    return (
        <div className='movie-id-summary'>
            <p>{props.summary}</p>
        </div> 
    )
};

export default MovieSummary;