import React, { useEffect, useState } from 'react';
import './searchInput.css';
import { FaSearch} from 'react-icons/fa'
import { NavLink } from 'react-router-dom';

const SearchInput = (props) => {
    const [inputValue, setInputValue] = useState('');
    const [searchRequest, setSearchRequest] = useState([]);
    const [show, setShow] = useState(false);
    const [id, setId] = useState('');

    useEffect(() => {
        const moviesToShow = props.allMovies.filter(el => {
            return el.name.toLowerCase().includes(inputValue.toLowerCase());
        })
        setSearchRequest(moviesToShow);
    },[inputValue]);

    const handleChange = e => {
        setInputValue(e.target.value);
    };

    let path = window.location.pathname.split('/')[1];

    return (
        <div className='search-wrapper'>
            <div>
                <input 
                    type='search' 
                    className='search-input' 
                    placeholder='Search...'
                    value={inputValue}
                    onChange={handleChange}
                    onClick={() => setShow(!show)}
                />
                {
                    path === '' ? 
                        <NavLink to={'./' + id + '/' + decodeURI(inputValue.toLowerCase().replace(/[:|&]/g, '').replace(/[^a-zA-Z0-9]/gi, '-'))} 
                            className="search-button" 
                        >
                                <span><FaSearch /> </span>
                                Search
                        </NavLink> :
                        <NavLink exact to={'.././' + id + '/' + decodeURI(inputValue.toLowerCase().replace(/[:|&]/g, '').replace(/[^a-zA-Z0-9]/gi, '-'))} 
                            className="search-button" 
                        >
                            <span><FaSearch /> </span>
                            Search
                        </NavLink>
                }
            </div>
            <div className='options-wrapper' >
                {
                    show && (
                        <div>
                            {
                                searchRequest.map(el => {
                                    if (path === '') {
                                        return (
                                            <p
                                                    key={el.name} 
                                                    className='options-search-results' 
                                                    onClick={() => {
                                                        setInputValue(el.name);
                                                        setId(el.id)
                                                    }}
                                            >
                                                    {el.name}
                                            </p>)
                                    } else {
                                        return (
                                            <p
                                                    key={el.name} 
                                                    className='options-search-results'
                                                    onClick={() => {
                                                        setInputValue(el.name);
                                                        setId(el.id)
                                                    }} 
                                            >
                                                    {el.name}
                                            </p>)
                                    }
                                       
                                })
                            }
                        </div>
                    )
                }
                
            </div>
        </div>
    )
};

export default SearchInput;