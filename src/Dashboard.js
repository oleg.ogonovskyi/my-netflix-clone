import React, { useEffect, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { useHistory } from "react-router";
import "./dashboard.css";
import { auth, db, logout } from "./firebase";
import {NavLink} from 'react-router-dom';

function Dashboard() {
  const [user, loading] = useAuthState(auth);
  const [name, setName] = useState("");
  const history = useHistory();

  const fetchUserName = async () => {
    try {
      const query = await db
        .collection("users")
        .where("uid", "==", user?.uid)
        .get();
      const data = await query.docs[0].data();
      setName(data.name);
    } catch (err) {
      console.error(err);
      alert("An error occured while fetching user data");
    }
  };

  useEffect(() => {
    if (loading) return;
    if (!user) return history.replace("/");

    fetchUserName();
  }, [user, loading]);
  
  return (
    <div className="dashboard">
      <div className="dashboard__container">
        Welcome, <span>{name}</span>
        <p>Enjoy our movies!</p>
        
        <NavLink to='/' onClick={() => localStorage.setItem('logedIn', true)}>
          <button className="dashboard__btn">
            Continue
          </button>
        </NavLink>
        <NavLink to='/' onClick={() => {
            localStorage.setItem('logedIn', false);
            return logout();
          }}>
          <button className="dashboard__btn" >
            Logout
          </button>
        </NavLink>
        
      </div>
    </div>
  );
}

export default Dashboard;