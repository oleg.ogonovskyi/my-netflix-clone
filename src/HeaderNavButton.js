import React from "react";
import MenuItem from "./MenuItem";
import Title from "./Title";

const HeaderNavButton = () => {
    return (
        <nav className='header-nav'>
            <Title />
            <MenuItem menuItemText='CRIME' />
            <MenuItem menuItemText='DRAMA' />
            <MenuItem menuItemText='THRILLER' />
            <MenuItem menuItemText='ROMANCE' />
            <MenuItem menuItemText='MEDICAL' />
            <MenuItem menuItemText='SUPERNATURAL' />
            <MenuItem menuItemText='ACTION' />
            <MenuItem menuItemText='SCIENCE-FICTION' />
            <MenuItem menuItemText='ADVENTURE' />
            <MenuItem menuItemText='FAMILY' />
        </nav>        
    )
};

export default HeaderNavButton;