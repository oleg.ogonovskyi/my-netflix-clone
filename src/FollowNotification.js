import React from 'react';
import './followedNotification.css';

const FollowNotification = (props) => {
    localStorage.setItem('favoriteMovie', props.movie);
    return (
        props.followed === 'Follow' ?
            <div className='followed-notification' style={{display: 'flex'}}>
                Congratulations! You're now following '{props.movie}'
            </div> :
            <div className='followed-notification'>
                ;( It's sad that you don't like this movie anymore!
            </div> 
    )
};

export default FollowNotification;