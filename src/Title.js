import React from "react";

const Title = () => {
    return (
        <h1 className='app-title'>FAKEFLIX</h1>
    )
};

export default Title;