import React from 'react';

const MoviePageTitle = (props) => {
    return (
        <h1 className='movie-title'>{props.title}</h1>
    )
};

export default MoviePageTitle;